#!/bin/bash

# ###################################
#
# This script is used to create a ROOT and INTERMEDIATE CA to sign CSRs from lower/sub CAs
#
# THIS IS FOR TESTING PURPOSES - NOT TO BE USED IN PRODUCTION ENVIRONMENTS
#
# TODO:
# Setup arguments to sign sub CA CSRs and server/host CSRs
#
# ###################################

DOMAINNAME="example.com"
EMAIL_ADDRESS="fakeemail@email.com"

CA_ROOT_DIR="/root/ca"
CA_INTERMEDIATE_DIR="$CA_ROOT_DIR/intermediate"

DL_ROOT_CONFIG="openssl_root.cnf"
DL_INTERMEDIATE_CONFIG="openssl_intermediate.cnf"

ROOT_CONFIG_FILE="$CA_ROOT_DIR/$DL_ROOT_CONFIG"
ROOT_KEY="$CA_ROOT_DIR/private/ca.${DOMAINNAME}.key.pem"
ROOT_CERT="$CA_ROOT_DIR/certs/ca.${DOMAINNAME}.crt.pem"

INTERMEDIATE_CONFIG_FILE="$CA_INTERMEDIATE_DIR/$DL_INTERMEDIATE_CONFIG"
INT_KEY="$CA_INTERMEDIATE_DIR/private/int.${DOMAINNAME}.key.pem"
INT_CERT="$CA_INTERMEDIATE_DIR/certs/int.${DOMAINNAME}.crt.pem"
INT_CSR="$CA_INTERMEDIATE_DIR/csr/int.${DOMAINNAME}.csr"

CHAIN_FILE="$CA_INTERMEDIATE_DIR/certs/chain.${DOMAINNAME}.crt.pem"

SERVER_CSR_CONFIG="$CA_INTERMEDIATE_DIR/openssl_csr_san.cnf"
SERVER_CSR="$CA_INTERMEDIATE_DIR/csr/${DOMAINNAME}.csr.pem"
SERVER_KEY="$CA_INTERMEDIATE_DIR/private/${DOMAINNAME}.key.pem"
SERVER_CERT="$CA_INTERMEDIATE_DIR/certs/${DOMAINNAME}.crt.pem"
SERVER_COMBINED="$CA_INTERMEDIATE_DIR/certs/${DOMAINNAME}.combined.pfx"

dir_tree () {
    # Setup root and intermediate ca directory tree
    if [ ! -d $CA_ROOT_DIR ]; then
        mkdir $CA_ROOT_DIR
    fi

    for SUBDIR in newcerts certs crl private requests; do
        if [ ! -d $CA_ROOT_DIR/$SUBDIR ]; then
            mkdir $CA_ROOT_DIR/$SUBDIR
        fi
    done

    if [ ! -d $CA_INTERMEDIATE_DIR ]; then
        mkdir $CA_INTERMEDIATE_DIR
    fi

    for SUBDIR in certs newcerts crl csr private; do
        if [ ! -d $CA_INTERMEDIATE_DIR/$SUBDIR ]; then
            mkdir $CA_INTERMEDIATE_DIR/$SUBDIR
        fi
    done
}

download_prereqs () {
    # Download required files/templates
    # wget -O $ROOT_CONFIG_FILE $DL_ROOT_CONFIG
    # wget -O $INTERMEDIATE_CONFIG_FILE $DL_INTERMEDIATE_CONFIG
    # wget -O $CA_ROOT_DIR/openssl_csr_san.cnf $DL_CSR
    # saved templates with code base - just need to move these to proper location
    cp $DL_ROOT_CONFIG $CA_ROOT_DIR
    cp $DL_INTERMEDIATE_CONFIG $CA_INTERMEDIATE_DIR
}

setup_root_ca () {
    # Create files for OpenSSL to keep track of all the signed certs and
    # serial file to give the start point for each signed certificate's serial number
    touch $CA_ROOT_DIR/index.txt
    touch $CA_ROOT_DIR/index.txt.attr
    echo '1000' > $CA_ROOT_DIR/serial

    # Modify root-ca configuration file
    sed -i "s/DOMAINNAME/$DOMAINNAME/g" $ROOT_CONFIG_FILE
    sed -i "s|^dir.*$|dir = $CA_ROOT_DIR |g" $ROOT_CONFIG_FILE

    # Generate a root private key then use private key to sign root certificate
    openssl genrsa -aes256 -out $ROOT_KEY 4096
    openssl req -config $ROOT_CONFIG_FILE -new -x509 -sha512 -extensions v3_ca \
    -key $ROOT_KEY \
    -out $ROOT_CERT \
    -days 3650 \
    -set_serial 0
}

setup_int_ca () {
    # Create files for OpenSSL to keep track of all the signed certs and
    # serial file to give the start point for each signed certificate's serial number
    touch $CA_INTERMEDIATE_DIR/index.txt
    touch $CA_INTERMEDIATE_DIR/index.txt.attr
    echo '1000' > $CA_INTERMEDIATE_DIR/crlnumber
    echo '1234' > $CA_INTERMEDIATE_DIR/serial

    # Modify intermediate config file
    sed -i "s/DOMAIN/$DOMAINNAME/g" $INTERMEDIATE_CONFIG_FILE
    sed -i "s|^dir.*$|dir = $CA_INTERMEDIATE_DIR |g" $INTERMEDIATE_CONFIG_FILE

    # Create private key and CSR (certificate signing request)
    openssl req -config $INTERMEDIATE_CONFIG_FILE -new -newkey rsa:4096 \
    -keyout $INT_KEY \
    -out $INT_CSR
    # Create intermediate certificate
    openssl ca -config $ROOT_CONFIG_FILE -extensions v3_intermediate_ca \
    -days 3650 -notext -md sha512 \
    -in $INT_CSR \
    -out $INT_CERT
}

create_chain_file () {
    cat $INT_CERT $ROOT_CERT > $CHAIN_FILE
}

create_server_csr_file () {
    # Server certificate signing request
    cat << EOF > $SERVER_CSR_CONFIG
[ req ]
default_bits       = 2048
distinguished_name = req_distinguished_name
req_extensions     = req_ext
prompt             = no

[ req_distinguished_name ]
countryName                = US
stateOrProvinceName        = TX
localityName               = San Antonio
organizationName           = SomeOrg
commonName                 = $DOMAINNAME

# Optionally, specify some defaults.
# countryName_default             = US
# stateOrProvinceName_default     = TX
# localityName_default            = San Antonio
# 0.organizationName_default      = SomeOrg
# organizationalUnitName_default  = unitthings
# emailAddress_default            = $EMAIL_ADDRESS

[ req_ext ]
subjectAltName = @alt_names

[alt_names]
DNS.1	= $DOMAINNAME
DNS.2	= www.${DOMAINNAME}
EOF
}

gen_server_certificate () {
    create_server_csr_file
    # Create private key and csr
    openssl req -out $SERVER_CSR \
    -newkey rsa:2048 -nodes \
    -keyout $SERVER_KEY \
    -config $SERVER_CSR_CONFIG
    # Sign the csr with the intermediate CA
    openssl ca -config $INTERMEDIATE_CONFIG_FILE \
    -extensions server_cert -days 3750 -notext -md sha512 \
    -in $SERVER_CSR \
    -out $SERVER_CERT
}

combined_certificate () {
    # Combined server key and cert into single pfx for windows IIS
    openssl pkcs12 \
    -inkey $SERVER_KEY \
    -in $SERVER_CERT -export \
    -out $SERVER_COMBINED

    # openssl pkcs12 \
    # -in www.example.com.combined.pfx -nodes \
    # -out $CA_INTERMEDIATE_DIR/certs/${DOMAINNAME}.combined.crt
}

output () {
    echo -e "\n=== Root CA ==="
    echo "KEY: $ROOT_KEY"
    echo "CERT: $ROOT_CERT"
    echo -e "\n=== Intermediate CA ==="
    echo "KEY: $INT_KEY"
    echo "CERT: $INT_CERT"
    # echo -e "\n=== Server ==="
    # echo "KEY: $SERVER_KEY"
    # echo "CERT: $SERVER_CERT"
}

main () {
    # Server Setup
    dir_tree
    download_prereqs
    # Create a chain of trust
    setup_root_ca
    setup_int_ca
    create_chain_file
    # Generate a server certificate
    # gen_server_certificate
    # combined_certificate
    output
}

main

# Sign lower/sub CA
# openssl ca -config $INTERMEDIATE_CA_CONFIG -extensions v3_intermediate_ca \
# -days 3650 -notext -md sha512 -in $PRIV_CA_CSR -out "priv_ca.crt.pem"